from fastapi import FastAPI
from fastapi.responses import JSONResponse

import requests

app = FastAPI()

id = ""

@app.get("/")
def readRoot():
    return("bienvenu sur le jeu")

@app.get("/creer-un-deck")
def creer_deck():
    global id
    request = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    json = request.json()
    id = json["deck_id"]

    return {"deck_id":id}


@app.post("/cartes/{nombre_cartes}")
def tirage_carte(nombre_cartes:int):
    global id
    creation_deck= False

    if (id == ""):
        creer_deck()
        creation_deck = True

    request = requests.get("https://deckofcardsapi.com/api/deck/"+id+"/draw/?count="+str(nombre_cartes))
    json = request.json()

    return {"deck_id": id,
        "création_deck": creation_deck, 
        "cartes": json["cards"]}

