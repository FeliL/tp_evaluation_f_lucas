import requests
import json

adresse = "http://127.0.0.1:8000/"

class id_deck:
    id:str
    def __init__(self,id):
        self.id=id

def get_id():
    req = requests.get(adresse+"creer-un-deck")
    r = req.json()["deck_id"]
    return(r)


def tirage_carte(nombre_cartes):
    data_json={"nombre_cartes": nombre_cartes}
    req = requests.post(adresse+"cartes/"+str(nombre_cartes), json=data_json)
    r = req.json()
    return(r)


def contage_carte(carte):
    tabl_carte = {"HEARTS":0,"SPADES":0,"DIAMONDS":0,"CLUBS":0}

    for i in carte["cartes"] :
        if i["suit"] ==  "HEARTS" :
            tabl_carte["HEARTS"] += 1 
        elif i["suit"] ==  "SPADES" :
            tabl_carte["SPADES"] += 1 
        elif i["suit"] ==  "DIAMONDS" :
            tabl_carte["DIAMONDS"] += 1 
        elif i["suit"] ==  "CLUBS" :
            tabl_carte["CLUBS"] += 1 
        
    return(tabl_carte)


nb = input("Entrez le nombre de cartes voulu : ")

crt = tirage_carte(nb)
tri_carte = contage_carte(crt)

print(tri_carte)


